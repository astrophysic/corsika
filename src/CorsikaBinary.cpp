#include "CorsikaBinary.hpp"
#include <cmath>
CorsikaBinary::CorsikaBinary(std::string fileName)
{
     binFile = new std::ifstream(fileName.c_str(), std::fstream::binary);
     binFile->seekg(0,ios::end);
     fileSize = binFile->tellg();
     binFile->seekg(0,ios::beg);
     binFile->seekg (4, std::ios::cur);
     currBlock = 0;
}

CorsikaBinary::~CorsikaBinary()
{
    for(auto&& particle : particles)
    {
        delete particle;
    }
}

void CorsikaBinary::ReadEventHeader(ifstream* file)
{

    float word;
    _4Vector _momentum;
    _4Vector _position;
    int type;

    // Número do evento
    file->read((char*)&word, 4);
    evth.eventNumber =  word;
    // ID do projétil
    file->read((char*)&word, 4);
    type = (int) word;
    // Energia do primário
    file->read((char*)&word, 4);
    _momentum[0] = word;
    // ID do alvo, se foi definido
    file->read((char*)&word, 4);
    evth.targetId = (int) word;
    
    // Altura do inicio do chuveiro
    file->read((char*)&word, 4);
    evth.startHeight = word;
    // Altura da primeira interação
    file->read((char*)&word, 4);
    evth.intHeight = word;
    _position[3] = word/100;

    // Momento do primário
    file->read((char*)&word, 4);
    _momentum[1] = word;
    file->read((char*)&word, 4);
    _momentum[2] = word;
    file->read((char*)&word, 4);
    _momentum[3] = word;
    // Inclinação do chuveiro
    file->read((char*)&word, 4);
    evth.theta = word;
    file->read((char*)&word, 4);
    evth.phi = word; 
    // Ignorando parte do cabeçalho
    file->seekg (248, std::ios::cur);
    // HI para baixa energia
    file->read((char*)&word, 4);
    evth.lowModel = (int) word; 
    // HI para baixa energia
    file->read((char*)&word, 4);
    evth.highModel = (int) word;
    // Ignorando parte do cabeçalho
    file->seekg (944, std::ios::cur);

    _position[0]=0;
    _position[1]=-1*_position[3]*tan(evth.theta)*cos(evth.phi);
    _position[2]=-1*_position[3]*tan(evth.theta)*sin(evth.phi);

    // Atualizando os valores da particula primária
    evth.primary.SetCorsikaParticle(_position,_momentum, type,1,0);
}

ParticleArray& CorsikaBinary::GetParticles()
{
    return this->particles;
}

CorsikaParticle& CorsikaBinary::GetPrimary()
{
    return this->evth.primary;
}

bool CorsikaBinary::NextShower()
{

    if (binFile->is_open())
    {
        // Variables to read a word of 4 bytes  
        char *line=new char[4];
        
        // Variable to read particle info
        float word;

        bool running = true;

        while(running){
            if(binFile->tellg() > fileSize){
                return false;
            }
            for (int j=currBlock; j < 21; j++){
                binFile->read(line, 4);
                line[4]='\0';

                if (strcmp(line,"EVTH")==0)
                {
                    ReadEventHeader(binFile);
                } 
                else if (strcmp(line,"EVTE")==0)
                {
                    binFile->seekg(1244,std::ios::cur);
                    currBlock = j;              
                    return true;
                    
                } 
                else if (strcmp(line,"RUNH")==0)
                {
                    binFile->seekg (1244, std::ios::cur);
                }
                else if (strcmp(line,"RUNE")==0)
                {
                    return false;
                }
                else  
                {
                    float info_part[8];
                    binFile->seekg(-4,std::ios::cur);
                    for (size_t i = 0; i < 39; i++) 
                    {
                        for(size_t j=0; j<8 ; j++)
                        {
                            binFile->read((char*)&info_part[j], 4);
                        }
                        // Testing if particle is muon
                        int type = (((int)info_part[0])/1000);
                        if (type > 0)
                        {
                            particles.push_back(new CorsikaParticle(info_part));
                        }
                    }
                }
            }
            binFile->seekg (4, std::ios::cur);
            binFile->seekg (4, std::ios::cur);
        }
        return false;
    }
}