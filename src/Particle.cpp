#include "Particle.hpp"
#include <cmath>

Particle::Particle()
{

}
Particle::Particle(Particle& other)
{
    this->momentum = other.momentum;
    this->position = other.position;
    this->type = other.type;
}
Particle::Particle(_4Vector& position, _4Vector& momentum, int type)
{
    this->momentum = momentum;
    this->position = position;
    this->type = type;
}

Particle& Particle::operator=(Particle& other)
{
    this->momentum = other.momentum;
    this->position = other.position;
    this->type = other.type;
}
_4Vector& Particle::GetMomentum()
{
    return this->momentum;
}
_4Vector& Particle::GetPosition()
{
    return this->position;
}
double Particle::GetType()
{
    return this->type;
}
double Particle::GetTheta()
{
    return atan2(this->momentum.Perp(),this->momentum[3]);
}
double Particle::GetPhi()
{
    return atan2(this->momentum[2],this->momentum[1]);
}
void Particle::SetMomentum(_4Vector& momentum)
{
    this->momentum = momentum;
}
void Particle::SetPosition(_4Vector& position)
{
    this->position = position;
}
void Particle::SetType(int type)
{
    this->type = type;
}