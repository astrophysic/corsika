#include "CorsikaParticle.hpp"
#include <cmath>

CorsikaParticle::CorsikaParticle() : Particle::Particle()
{

}

CorsikaParticle::CorsikaParticle(CorsikaParticle& other)
{
    this->momentum = other.momentum;
    this->position = other.position;
    this->type = other.type;
    this->generation = other.generation;
    this->thinning = other.thinning;
}

CorsikaParticle::CorsikaParticle(_4Vector& position, _4Vector& momentum, int type, double thinning, double generation)
: Particle::Particle(position, momentum, type)
{
    this->thinning = thinning;
    this->generation = generation;
}

CorsikaParticle::CorsikaParticle(float* binData)
{
      
    this->type = ((int)binData[0]/1000);

    //Calculate Generation from ID input
    this->generation=(((int)binData[0])%1000);
    if (this->generation > 500 ){
        this->generation=(int)(this->generation-500)/10;
    } else {
        this->generation=(int)this->generation/10;
    }

    this->momentum[1] = binData[1];
    this->momentum[2] = binData[2];
    this->momentum[3] = binData[3];

    this->momentum[0] = sqrt(pow(momentum[1],2)+ pow(momentum[2],2)+pow(momentum[3],2));

    this->position[1] = binData[4]/100;
    this->position[2] = binData[5]/100;
    this->position[3] = 0;
    this->position[0] = binData[6];

    //Thinning
    this->thinning=binData[7];
}

CorsikaParticle& CorsikaParticle::operator=(CorsikaParticle& other)
{
    this->momentum = other.momentum;
    this->position = other.position;
    this->type = other.type;
    this->generation = other.generation;
    this->thinning = other.thinning;
}

void CorsikaParticle::SetCorsikaParticle(_4Vector& position, _4Vector& momentum, int type, double thinning, double generation){
        this->thinning = thinning;
        this->generation = generation;
        this->momentum = momentum;
        this->position = position;
        this->type = type;
}

double CorsikaParticle::GetThinning()
{
    return this->thinning;
}

double CorsikaParticle::GetGeneration()
{
    return this->generation;
}

void CorsikaParticle::SetThinning(double thinning)
{
    this->thinning = thinning;
}

void CorsikaParticle::SetGeneration(double generation)
{
    this->generation = generation;
}