#include "_4Vector.hpp"
#include <cmath>

_4Vector::_4Vector()
{

}
_4Vector::_4Vector(_4Vector& other)
{
    for(size_t i = 0; i < 4; i++)
    {
        this->array[i] = other.array[i];
    }
    
}
_4Vector::_4Vector(double t, double x, double y, double z)
{
    this->array[0] = t;
    this->array[1] = x;
    this->array[2] = y;
    this->array[3] = z;
}

_4Vector& _4Vector::operator=(_4Vector& other)
{
    for(size_t i = 0; i < 4; i++)
    {
        this->array[i] = other.array[i];
    }
}

double& _4Vector::operator[](size_t i)
{
    return this->array[i];
}
double _4Vector::operator[](size_t i) const
{
    return this->array[i];
}

double _4Vector::Perp()
{
    return sqrt(this->array[1]*this->array[1]+
                this->array[2]*this->array[2]);
}