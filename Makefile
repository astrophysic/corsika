SRCDIR := src
OBJDIR := lib
INCDIR := inc

SRCS := $(wildcard $(SRCDIR)/*cpp)
OBJS := $(addprefix $(OBJDIR)/, $(notdir $(SRCS:.cpp=.o)))

INC := -Iinc
LIB := 
CFLAGS := -g -std=c++17
CLIBS :=

CC := g++

.PHONY: all
all: $(OBJDIR)/libCorsika.a 

$(OBJDIR)/libCorsika.a: $(OBJS) 
	ar rvs -o $@ $^ 

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp | $(OBJDIR)/
	$(CC) $(CFLAGS) -c $< -o $@ $(INC)

$(OBJDIR)/: 
	mkdir -p $@

clean:
	rm -rf lib/*.o
	rm -rf lib/*.a
