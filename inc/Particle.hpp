#ifndef _PARTICLE_HPP_
#define _PARTICLE_HPP_

#include <iostream>
#include "_4Vector.hpp"

class Particle 
{
protected:
    _4Vector position;
    _4Vector momentum;
    int type;
public:
    Particle();
    Particle(Particle& other);
    Particle(_4Vector& position, _4Vector& momentum, int type);

    Particle& operator=(Particle& other);
    _4Vector& GetMomentum();
    _4Vector& GetPosition();
    double GetType();
    double GetTheta();
    double GetPhi();

    void SetMomentum(_4Vector& momentum);
    void SetPosition(_4Vector& position);
    void SetType(int type);
};

#endif