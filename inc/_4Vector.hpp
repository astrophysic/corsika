#ifndef _4VECTOR_HPP_
#define _4VECTOR_HPP_

#include <iostream>

class _4Vector 
{
    double array[4];
public:
    _4Vector();
    _4Vector(_4Vector& other);
    _4Vector(double t, double x, double y, double z);

    _4Vector& operator=(_4Vector& other);
    double& operator[](size_t i);
    double operator[](size_t i) const;
    double Perp();
};

#endif