#ifndef _CORSIKA_BINARY_HPP_
#define _CORSIKA_BINARY_HPP_

#include <iostream>
#include <cstring>
#include <vector>
#include <fstream>

#include "CorsikaParticle.hpp"

using namespace std;

struct EventHeader {
    CorsikaParticle primary;
    size_t eventNumber;
    double startHeight;
    double targetId;
    double intHeight;
    size_t highModel;
    size_t lowModel;
    double theta;
    double phi;
};


class CorsikaBinary
{
    EventHeader evth;
    ParticleArray particles;
    ifstream* binFile;

    size_t currBlock;
    size_t fileSize;
public:
    CorsikaBinary(string fileName);
    ~CorsikaBinary();
    void ReadEventHeader(ifstream* file);
    ParticleArray& GetParticles();
    CorsikaParticle& GetPrimary();
    bool NextShower();
};

#endif