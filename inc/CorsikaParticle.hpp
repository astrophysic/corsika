#ifndef _CORSIKA_PARTICLE_HPP_
#define _CORSIKA_PARTICLE_HPP_

#include <iostream>
#include <vector>

#include "Particle.hpp"

class CorsikaParticle  : public Particle
{
protected:
    double thinning;
    double generation;
public:
    CorsikaParticle();
    CorsikaParticle(CorsikaParticle& other);
    CorsikaParticle(float* binData);
    CorsikaParticle(_4Vector& position, _4Vector& momentum, int type, double thinning, double generation);

    CorsikaParticle& operator=(CorsikaParticle& other);

    double GetThinning();
    double GetGeneration();

    void SetThinning(double thinning);
    void SetGeneration(double generation);
    void SetCorsikaParticle(_4Vector& position, _4Vector& momentum, int type, double thinning, double generation);

    
};

typedef std::vector<CorsikaParticle*> ParticleArray;

#endif